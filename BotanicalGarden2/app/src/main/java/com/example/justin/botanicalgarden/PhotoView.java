package com.example.justin.botanicalgarden;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class PhotoView extends AppCompatActivity {
    String picPath = null;
    ImageView cameraImage = null;
    TextView photoText = null;
    Bitmap thumbnail = null;
    private static final int CAM_REQUEST = 1313;
    Firebase datFire;
    int childCount = 0;
    String [] imageBitArray;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view);
        System.gc();
        setTitle("Photo Upload and Gallery");
        Firebase.setAndroidContext(this);
        datFire = new Firebase("https://uncc-botanicalgarden.firebaseio.com/");
        cameraImage = (ImageView) findViewById(R.id.testImage);
        photoText = (TextView) findViewById(R.id.textView3);

        Button cameraUpload = (Button) findViewById(R.id.cameraUpload);
        cameraUpload.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAM_REQUEST);

            }

        });

//        Button upload = (Button) findViewById(R.id.upload);
//        upload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    Intent uploadPhoto = new Intent();
//                    uploadPhoto.setType("image/*");
//                    uploadPhoto.setAction(Intent.ACTION_GET_CONTENT);
//                    startActivityForResult(
//                            Intent.createChooser(uploadPhoto, "Select Picture"),
//                            1);
//                } catch (Exception e) {
//                    Toast.makeText(getApplicationContext(),
//                            e.getMessage(),
//                            Toast.LENGTH_LONG).show();
//                }
//            }
//        });


        Button serverUpload = (Button) findViewById(R.id.uploadChoice);
        serverUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Bitmap mapBit = BitmapFactory.decodeResource(getResources(), R.id.testImage);
                ByteArrayOutputStream dat = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.PNG, 100, dat);
                thumbnail = null;
                byte[] datArray = dat.toByteArray();
                String datImage = Base64.encodeToString(datArray, Base64.DEFAULT);


                Firebase postReference = datFire.child("StringMap");
                Firebase newPostReference = postReference.push();

                Map<String, String> map = new HashMap<String, String>();

                map.put("image", datImage);

                newPostReference.setValue(map);

                Toast.makeText(PhotoView.this, "Photo Uploaded", Toast.LENGTH_LONG).show();
            }
        });

        Button gallery = (Button) findViewById(R.id.viewGallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cameraImage = null;
                Intent galleryPush = new Intent(PhotoView.this, GalleryActivity.class);
                startActivity(galleryPush);
                //Toast.makeText(PhotoView.this, datImage, Toast.LENGTH_LONG).show();
            }
        });


    }

        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            Uri selectedImageUri = null;
            Bundle bund = data.getExtras();

            //Log.d("Path","mypath="+filePath );
            if (resultCode == Activity.RESULT_OK) {
                selectedImageUri = data.getData();
                Log.d("URI", "=" + selectedImageUri);
                try {
//                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
//                    ((ImageView) findViewById(R.id.testImage)).setImageBitmap(bitmap);
//                    getPath(data.getData());
                    thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);

                    ExifInterface exif = new ExifInterface(selectedImageUri.getPath());
                    int position = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    Matrix mat = new Matrix();
                    mat.postRotate(90);
                    thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), mat, true);

                    cameraImage.setImageBitmap(thumbnail);
                    getPath(data.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if(requestCode == CAM_REQUEST) {
                thumbnail = (Bitmap) data.getExtras().get("data");
                cameraImage.setImageBitmap(thumbnail);

                if(thumbnail != null) {
                    cameraImage.setBackgroundColor(0);
                    photoText.setText("");
                }

            }
        }

        public void getPath(Uri uri) {

            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = managedQuery(uri, projection, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            picPath = cursor.getString(column_index);
            //return cursror.getString(column_index);
        }


   }

