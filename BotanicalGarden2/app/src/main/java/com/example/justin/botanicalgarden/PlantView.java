package com.example.justin.botanicalgarden;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class PlantView extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_view);

        final String whereFrom = getIntent().getStringExtra("Uniqid");
        final String[] plantNames = getIntent().getStringArrayExtra("PlantNames");
        final int scrolledOffset = getIntent().getIntExtra("ScrolledOffset", 0);

        Object[] objectArray = (Object[]) getIntent().getExtras().getSerializable("PlantPositions");
        final int[][] plantPositions = new int[objectArray.length][2];
        for(int i = 0; i< objectArray.length; i++) {
            plantPositions[i] = (int[])objectArray[i];
        }

        Object[] objectArray2 = (Object[]) getIntent().getExtras().getSerializable("PlantEnds");
        final int[][] plantEnds = new int[objectArray2.length][2];
        for(int i = 0; i < objectArray2.length; i++) {
            plantEnds[i] = (int[])objectArray2[i];
        }

        /*ImageView plantImage = (ImageView)findViewById(R.id.imageView4);
        plantImage.setImageResource(R.drawable.uncc_cone_entry1);*/


        Button enter = (Button) findViewById(R.id.button);
        enter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(PlantView.this, ScrolledPan.class);
                intent.putExtra("Uniqid", whereFrom);
                intent.putExtra("PlantNames", plantNames);
                Bundle returnBundle = new Bundle();
                returnBundle.putSerializable("PlantPositions", plantPositions);
                returnBundle.putSerializable("PlantEnds", plantEnds);
                intent.putExtras(returnBundle);
                intent.putExtra("ScrolledOffset", scrolledOffset);
                startActivity(intent);

            }

        });
    }
}
