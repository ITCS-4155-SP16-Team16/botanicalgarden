package com.example.justin.botanicalgarden;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends AppCompatActivity {

    Firebase firebaseref;
    Firebase.AuthResultHandler authResultHandler;
    EditText email, pw, confirmpw, name;
    Button signUpNow;
    String emailString, pwString, pwConfirmString, nameString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Firebase.setAndroidContext(this);
        firebaseref = new Firebase("https://uncc-botanicalgarden.firebaseio.com/");
        name = (EditText) findViewById(R.id.editText);
        email = (EditText) findViewById(R.id.editText2);
        pw = (EditText) findViewById(R.id.editText3);
        confirmpw = (EditText) findViewById(R.id.editText4);
        signUpNow = (Button) findViewById(R.id.button2);
        signUpNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailString = email.getText().toString();
                pwString = pw.getText().toString();
                pwConfirmString = confirmpw.getText().toString();
                nameString = name.getText().toString();
                if (emailString.trim().equals("") || pwString.trim().equals("") || pwConfirmString.trim().equals("") || nameString.trim().equals("")) {
                    Toast.makeText(SignupActivity.this, "You must enter all fields!", Toast.LENGTH_SHORT).show();
                } else {
                    if (!pwString.equals(pwConfirmString)) {
                        Toast.makeText(SignupActivity.this, "Passwords do not match", Toast.LENGTH_SHORT).show();
                    }else{
                        Firebase.setAndroidContext(SignupActivity.this);
                        firebaseref.authWithPassword(emailString, pwString, authResultHandler);
                        firebaseref.createUser(emailString, pwString, new Firebase.ValueResultHandler<Map<String, Object>>() {
                            @Override
                            public void onSuccess(Map<String, Object> result) {

                                Toast.makeText(SignupActivity.this, "Account Created", Toast.LENGTH_SHORT).show();
                                Map<String, String> map = new HashMap<String, String>();
                                Profile profile = new Profile();
                                profile.setEmail(emailString);
                                profile.setPassword(pwString);
                                profile.setname(nameString);
                                map.put("email", profile.getEmail());
                                map.put("Name", profile.getname());
                                map.put("password", profile.getPassword());
                                Map<String, Map<String, String>> profiles = new HashMap<String, Map<String, String>>();
                                profiles.put(profile.getname(), map);
                                firebaseref.child("profile").setValue(profile);
                                Toast.makeText(SignupActivity.this, "User is added", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }

                            @Override
                            public void onError(FirebaseError firebaseError) {
                                switch (firebaseError.getCode()) {
                                    case FirebaseError.EMAIL_TAKEN:
                                        Toast.makeText(SignupActivity.this, "Email is already present.", Toast.LENGTH_SHORT).show();
                                        break;
                                    case FirebaseError.INVALID_EMAIL:
                                        Toast.makeText(SignupActivity.this, "Check your email", Toast.LENGTH_SHORT).show();
                                        break;
                                    case FirebaseError.INVALID_PASSWORD:
                                        Toast.makeText(SignupActivity.this, "Invalid Password.", Toast.LENGTH_SHORT).show();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        });

                    }
                }
            }
        });
    }
}








