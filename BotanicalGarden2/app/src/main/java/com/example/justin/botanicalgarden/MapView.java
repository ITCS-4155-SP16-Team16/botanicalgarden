package com.example.justin.botanicalgarden;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MapView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_view);

        Button enter = (Button) findViewById(R.id.returnHome);
        enter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MapView.this, MainActivity.class);
                startActivity(intent);

            }

        });

    }

    String[] tile1Plants = new String[] {"DistylumMyricoides", "Stachyurusprarcox", "MagnoliaMacrophylla", "EdgeworthiaChrysantha1", "NandinaDomestica", "EdgeworthiaChrysantha2", "LirlopeMuscari1", "AesculusHippocastanum", "ViburnumCarlesii", "LirlopeMuscari2", "NeriumOleander", "DwarfPicea", "JuniperusProcumbens", "OphiopogonJaponicus", "AcerPalmatum", "EuryaJaponicus", "AzaleaAutumn", "Nandina1", "BuxusKingsville", "UlmusAlata", "AcertriflorumThree", "NandineDomestic", "BuxusX", "MagnoliaYuyuanensis", "EdgeworthiaChrysantha3", "Nandina2", "CornusFlorida", "PrunusmumeHokkai", "DistylumMyricoides2"};
    int[][] tile1Positions = new int [][] {{0, 493}, {119, 387}, {260, 225}, {390,399}, {320, 557}, {519, 347}, {626, 614}, {672, 125}, {863, 370}, {836, 571}, {1021, 480}, {410, 670}, {528, 705}, {992, 713}, {1186, 309}, {1310, 443}, {1302, 547}, {1247, 612}, {1302, 609}, {1445, 476}, {1550, 364}, {1669, 649}, {1934, 497}, {2022, 233}, {2148, 550}, {2286, 836}, {2423, 174}, {2356, 412}, {2494, 537}};
    int[][] tile1Ends = new int [][] {{140, 520}, {260, 440}, {485, 270}, {545, 435}, {455, 595}, {680, 390}, {815, 655}, {880, 190}, {990, 415}, {1015, 615}, {1150, 515}, {515, 705}, {680, 740}, {1160, 750}, {1230, 345}, {1420, 475}, {1400, 575}, {1300, 650}, {1400, 645}, {1545, 530}, {1685, 405}, {1800, 695}, {2035, 540}, {2170, 275}, {2315, 595}, {2365, 865}, {2525, 235}, {2465, 460}, {2645, 580}};
    String[] tile2Plants = new String[] {"IlliciumHenryi", "EuryaJaponica", "HamamelisIntermedia", "IllciumFlorida", "AcerBarbatum", "BigoniaCapreolata", "PyracanthaCoccinea", "CrinumSp", "PiceaOrientalis1", "AcerPalmatum1", "AjugaReptans", "ChrysogonumVirginianum", "RhododendronSerpyllifolium", "PrunusIusitanica", "RhododendronDwarf", "AcerPalmatum2", "CyperusPhyllocephala", "CamelliaSasangua", "MagnoliaFigo", "Hypericum", "CamelliaSasMisty", "PiceaOrientalis2", "RhododendronAzalea1", "RhododendronAzalea2"};
    int[][] tile2Positions = new int[][] {{0, 375},{0, 450}, {115, 365}, {390, 370}, {500, 95}, {605, 325}, {530, 610}, {905, 605}, {1080, 265}, {980, 485}, {1005, 780}, {1125, 625}, {1230, 505}, {1340, 355}, {1340, 550}, {1460, 390}, {1555, 675}, {1635, 450}, {1790, 360}, {1850, 610}, {2160, 465}, {2440, 235}, {2405, 630}, {2545, 500}};
    int[][] tile2Ends = new int[][] {{110, 425}, {95, 495}, {335, 420}, {530, 415}, {620, 145}, {740, 385}, {675, 670}, {1000, 655}, {1200, 295}, {1145, 530}, {1115, 825}, {1380, 665}, {1375, 560}, {1505, 400}, {1445, 600}, {1640, 455}, {1715, 720}, {1765, 490}, {1900, 405}, {1950, 645}, {2300, 515}, {2565, 280}, {2570, 660}, {2700, 530}};
    String[] tile3Plants = new String[] {"AndropogonTenarius", "AcorusGramineus", "CallicarpaDichotoma", "ChamaecyparisObtusa", "NymphaeaYellow", "KalmiaLatifolia", "CryptomeriaJaponica", "AcerPalmatum", "SciadopitysVerticillata", "ReincekiaCarnea", "AbiesFirma", "ClethraAlnifolia", "LoniceraNitida", "AcerBuergeranum", "TaxodiumDistichum", "IrisEnsata", "Taxus", "FothergillaMt", "Sinojackia", "NandinaDomestica", "PolygonatumOdoratissium", "BambusaMultiples", "GordliniaGrandiflora", "ViburnumPlicatum", "PrunusLauvocerasus", "PierisJaponica", "BuxusSemper"};
    int[][] tile3Positions = new int[][] {{18, 642}, {117, 565}, {200, 373}, {246, 472}, {297, 631}, {372, 481}, {562, 240}, {571, 409}, {736, 223}, {757, 469}, {858, 300}, {966, 456}, {1032, 553}, {1155, 115}, {1110, 445}, {1155, 620}, {1348, 444}, {1636, 442}, {1901, 393}, {1939, 496}, {1849, 616}, {2095, 471}, {2224, 507}, {2416, 331}, {2446, 450}, {2479, 522}, {2617, 472}};
    int[][] tile3Ends = new int[][] {{170, 685}, {245, 615}, {345, 420}, {367, 530}, {430, 670}, {495, 525}, {715, 290}, {700, 485}, {850, 290}, {905, 515}, {945, 350}, {1080, 510}, {1165, 605}, {1295, 165}, {1255, 495}, {1230, 660}, {1405, 490}, {1720, 495}, {2000, 435}, {2095, 545}, {2050, 690}, {2230, 525}, {2380, 555}, {2650, 385}, {2595, 505}, {2600, 575}, {2685, 535}};

    public void toScrolledPan(View v) {
        switch (v.getId()) {
            case (R.id.tile1Button):
                Intent tile1Intent = new Intent(MapView.this, ScrolledPan.class);
                tile1Intent.putExtra("Uniqid", "tile_1_pan");
                tile1Intent.putExtra("PlantNames", tile1Plants);
                Bundle plantBundle1 = new Bundle();
                plantBundle1.putSerializable("PlantPositions", tile1Positions);
                plantBundle1.putSerializable("PlantEnds", tile1Ends);
                tile1Intent.putExtras(plantBundle1);
                tile1Intent.putExtra("ScrolledOffset", 0);
                startActivity(tile1Intent);
                break;

            case (R.id.tile2Button):
                Intent tile2Intent = new Intent(MapView.this, ScrolledPan.class);
                tile2Intent.putExtra("Uniqid", "tile_2_pan");
                tile2Intent.putExtra("PlantNames", tile2Plants);
                Bundle plantBundle2 = new Bundle();
                plantBundle2.putSerializable("PlantPositions", tile2Positions);
                plantBundle2.putSerializable("PlantEnds", tile2Ends);
                tile2Intent.putExtras(plantBundle2);
                tile2Intent.putExtra("ScrolledOffset", 0);
                startActivity(tile2Intent);
                break;

            case (R.id.tile3Button):
                Intent tile3Intent = new Intent(MapView.this, ScrolledPan.class);
                tile3Intent.putExtra("Uniqid", "tile_3_pan");
                tile3Intent.putExtra("PlantNames", tile3Plants);
                Bundle plantBundle3 = new Bundle();
                plantBundle3.putSerializable("PlantPositions", tile3Positions);
                plantBundle3.putSerializable("PlantEnds", tile3Ends);
                tile3Intent.putExtras(plantBundle3);
                tile3Intent.putExtra("ScrolledOffset", 0);
                startActivity(tile3Intent);
                break;

        }
    }


}
