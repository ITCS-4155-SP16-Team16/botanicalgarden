package com.example.justin.botanicalgarden;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import java.util.Arrays;

public class ScrolledPan extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolled_pan);
        System.gc();
        final HorizontalScrollView scrollView = (HorizontalScrollView) findViewById(R.id.horizontalScrollView);
        scrollView.setHorizontalScrollBarEnabled(false);

        final String whereFrom = getIntent().getStringExtra("Uniqid");
        final String[] plantNames = getIntent().getStringArrayExtra("PlantNames");

        switch(whereFrom) {
            case("tile_1_pan"):
                this.setTitle("Tile 1, select a plant to view");
                break;
            case("tile_2_pan"):
                this.setTitle("Tile 2, select a plant to view");
                break;
            case("tile_3_pan"):
                this.setTitle("Tile 3, select a plant to view");
                break;
        }

        Object[] objectArray = (Object[]) getIntent().getExtras().getSerializable("PlantPositions");
        final int[][] plantPositions = new int[objectArray.length][2];
        for(int i = 0; i< objectArray.length; i++) {
            plantPositions[i] = (int[])objectArray[i];
        }

        Object[] objectArray2 = (Object[]) getIntent().getExtras().getSerializable("PlantEnds");
        final int[][] plantEnds = new int[objectArray2.length][2];
        for(int i = 0; i < objectArray2.length; i++) {
            plantEnds[i] = (int[])objectArray2[i];
        }


        int resID = getResources().getIdentifier(whereFrom, "drawable", getPackageName());
        ImageView panImgView = (ImageView) findViewById(R.id.panImgView);
        Drawable drawable = getResources().getDrawable(resID);
        panImgView.setImageDrawable(drawable);

        View.OnClickListener plantListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Object tag = v.getTag();
                final int scrollLocation = scrollView.getScrollX();

                Intent intent = new Intent(ScrolledPan.this, PlantView.class);
                intent.putExtra("Uniqid", whereFrom);
                intent.putExtra("PlantNames", plantNames);
                Bundle returnBundle = new Bundle();
                returnBundle.putSerializable("PlantPositions", plantPositions);
                returnBundle.putSerializable("PlantEnds", plantEnds);
                intent.putExtras(returnBundle);
                intent.putExtra("ScrolledOffset", scrollLocation);
                startActivity(intent);
            }
        };

        RelativeLayout rl = (RelativeLayout)findViewById(R.id.scrolledRL);
        Button buttonArray[] = new Button[plantNames.length];
        for(int i = 0; i < buttonArray.length; i++) {
            buttonArray[i] = new Button(this);
            buttonArray[i].setX(plantPositions[i][0]);
            buttonArray[i].setY(plantPositions[i][1]);
            buttonArray[i].setWidth(plantEnds[i][0] - plantPositions[i][0]);
            buttonArray[i].setHeight(plantEnds[i][1] - plantPositions[i][1]);
            buttonArray[i].setAlpha(0);

            rl.addView(buttonArray[i]);

            buttonArray[i].setOnClickListener(plantListener);
            buttonArray[i].setTag(plantNames[i]);
        }

        /*Button enter = (Button) findViewById(R.id.toPlant);
        enter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ScrolledPan.this, PlantView.class);
                intent.putExtra("Uniqid", whereFrom);
                startActivity(intent);

            }

        });*/

        Button enter = (Button) findViewById(R.id.mapReturn);
        enter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ScrolledPan.this, MapView.class);
                startActivity(intent);

            }

        });

        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(getIntent().getIntExtra("ScrolledOffset", 0),0);
            }
        });
    }
}
