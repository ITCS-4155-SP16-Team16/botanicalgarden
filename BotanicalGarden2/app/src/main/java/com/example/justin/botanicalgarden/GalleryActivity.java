package com.example.justin.botanicalgarden;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.media.*;
import android.media.ImageReader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Gallery;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;


public class GalleryActivity extends AppCompatActivity {
    ImageView displayPhoto;
    Firebase datFire;
    int childCount;
    String [] imageBitArray;
    String [] keyArray;
    ImageView imageArray[];
    int thisChild = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        System.gc();
        setTitle("User Photo Gallery");
        Firebase.setAndroidContext(this);
        datFire = new Firebase("https://uncc-botanicalgarden.firebaseio.com/");

        datFire.child("StringMap").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //String numberAsString = Long.toString(dataSnapshot.getChildrenCount());
                childCount = (int) dataSnapshot.getChildrenCount();
                imageBitArray = new String[childCount];
                keyArray = new String[childCount];
                RelativeLayout rl = (RelativeLayout) findViewById(R.id.scrolledRelativeLayout);



                dataSnapshot.getChildren();
                int counter = 0;
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String image = postSnapshot.getValue().toString();
                    //Log.d("demo", image);
                    imageBitArray[counter] = image;
                    //Log.d("imageArrayGallery", imageBitArray[counter]);
                    //Log.d("FBkey", String.valueOf(postSnapshot.getKey()));
                    keyArray[counter] = String.valueOf(postSnapshot.getKey());

                    counter++;

                }
                imageArray = new ImageView[imageBitArray.length];
                //Log.d("Length", String.valueOf(imageArray.length));

                for (int i = 0, w = 0, h = 0; i < imageBitArray.length; i++) {
                    imageBitArray[i] = imageBitArray[i].replace("{image=", "");
                    //Log.d("replace", imageBitArray[i]);
                    byte[] data = Base64.decode(imageBitArray[i], Base64.DEFAULT);
                    Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);

                    imageArray[i] = new ImageView(GalleryActivity.this);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(205, 360);
                    layoutParams.setMargins(0, 0, 0, 0);
                    imageArray[i].setLayoutParams(layoutParams);
                    imageArray[i].setX(w * 215);
                    imageArray[i].setY(h * 361);


                    imageArray[i].setImageBitmap(bmp);
                    imageArray[i].setClickable(true);

                    rl.addView(imageArray[i]);

                    w++;
                    //Log.d("iTag", String.valueOf(i));
                    if (w > 2) {
                        h++;
                        w = 0;
                        // rl.setMinimumHeight();
                    }
                    if ((i % 3) == 0)
                    {
                        rl.getLayoutParams().height = h * 430;
                    }

                    imageArray[i].setOnClickListener(imageListener);
                    imageArray[i].setTag(String.valueOf(i));



                }


            }


            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });




    }

    View.OnClickListener imageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Object tag = v.getTag();

            Intent intent = new Intent(GalleryActivity.this, SelectedPhoto.class);
            intent.putExtra("image", keyArray[Integer.valueOf(String.valueOf(tag))]);
            startActivity(intent);
        }
    };
}
