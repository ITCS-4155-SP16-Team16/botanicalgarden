package com.example.justin.botanicalgarden;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

public class LoginActivity extends AppCompatActivity {

    EditText email, password;
    Firebase.AuthResultHandler authResultHandler;
    Firebase firebaseref;
    Button loginClick;
    String emailString, passwordString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Firebase.setAndroidContext(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Login");
        firebaseref = new Firebase("https://uncc-botanicalgarden.firebaseio.com/");
        Firebase.setAndroidContext(this);
        email = (EditText) findViewById(R.id.editText);
        password = (EditText) findViewById(R.id.editText2);


        loginClick = (Button) findViewById(R.id.userLogin);

        loginClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailString = email.getText().toString();
                passwordString = password.getText().toString();
                firebaseref.authWithPassword(emailString, passwordString, authResultHandler);
            }
        });

        Button signUp = (Button) findViewById(R.id.button3);
        signUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent signupActivity = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(signupActivity);
            }

        });


        authResultHandler = new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                Toast.makeText(LoginActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                Firebase.setAndroidContext(getBaseContext());
                Intent webPhoto = new Intent(LoginActivity.this, PhotoView.class);
                startActivity(webPhoto);


            }

            public void onAuthenticationError(FirebaseError firebaseError) {
                switch (firebaseError.getCode()) {
                    case FirebaseError.EMAIL_TAKEN:
                        Toast.makeText(LoginActivity.this, "Email is already present.", Toast.LENGTH_SHORT).show();
                        break;
                    case FirebaseError.INVALID_EMAIL:
                        Toast.makeText(LoginActivity.this, "Check your email", Toast.LENGTH_SHORT).show();
                        break;
                    case FirebaseError.INVALID_PASSWORD:
                        Toast.makeText(LoginActivity.this, "Invalid Password.", Toast.LENGTH_SHORT).show();
                        break;
                    case FirebaseError.USER_DOES_NOT_EXIST:
                        Toast.makeText(LoginActivity.this, "User does not exist", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }

            }
        };
    }
}


