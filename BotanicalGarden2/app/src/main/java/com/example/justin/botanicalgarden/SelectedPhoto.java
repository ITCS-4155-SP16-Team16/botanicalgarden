package com.example.justin.botanicalgarden;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class SelectedPhoto extends Activity {
    ImageView display = null;
    Firebase datFire;
    String image;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_photo);
        display = (ImageView) findViewById(R.id.imageView2);
        final String pictureKey = getIntent().getStringExtra("image");
        Firebase.setAndroidContext(this);
        datFire = new Firebase("https://uncc-botanicalgarden.firebaseio.com/");


        datFire.child("StringMap").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    //Log.d("pictureKey", pictureKey);

                    if (pictureKey.equals(postSnapshot.getKey().toString()))
                    {
                        //Log.d("equal", "Equal here!!!!!!!!!!!!!!!!!!!!!!!!!");
                    }
                    //Log.d("snapshotKey", postSnapshot.getKey().toString());

                    if(postSnapshot.getKey().toString().equals(pictureKey))
                    {
                        image = postSnapshot.getValue().toString();
                        //Log.d("imageKey", image);
                        image = image.replace("{image=", "");
                        //Log.d("replaceKey", image);
                        byte[] data = Base64.decode(image, Base64.DEFAULT);
                        Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                        display = (ImageView) findViewById(R.id.imageView2);
                        display.setImageBitmap(bmp);
                    }
                }



            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });





        }
    }
